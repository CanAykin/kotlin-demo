FROM openjdk:8-jdk-alpine
ENV TZ Europe/Berlin
VOLUME /tmp
COPY build/libs/*.jar kotlin-demo.jar
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/kotlin-demo.jar"]
