package de.iits.education.kotlindemo.data.repo.pricing
import de.iits.education.kotlindemo.data.pricing.PricingDetail
import de.iits.education.kotlindemo.data.ProjectMasterData
import de.iits.education.kotlindemo.data.Project
import org.springframework.data.repository.CrudRepository;

interface PricingDetailRepository : CrudRepository<PricingDetail, Long>