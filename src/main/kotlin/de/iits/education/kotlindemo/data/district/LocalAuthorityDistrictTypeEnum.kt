package de.iits.education.kotlindemo.data.district

enum class LocalAuthorityDistrictTypeEnum(val germanLabel : String) {
    COMMUNITY("Gemeinde"),
    CITY("Stadt");
}
