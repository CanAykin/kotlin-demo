package de.iits.education.kotlindemo.data.repo.pricing

import de.iits.education.kotlindemo.data.Project
import de.iits.education.kotlindemo.data.ProjectMasterData
import de.iits.education.kotlindemo.data.pricing.PricingMasterData
import org.springframework.data.repository.CrudRepository

interface PricingMasterDataRepository : CrudRepository<PricingMasterData, Long>