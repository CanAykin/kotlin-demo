package de.iits.education.kotlindemo.data.repo.district

import de.iits.education.kotlindemo.data.district.ContactPerson
import org.springframework.data.repository.CrudRepository


interface ContactPersonRepository : CrudRepository<ContactPerson, Long>