package de.iits.education.kotlindemo.data.pricing


import javax.persistence.CascadeType.ALL

import java.math.BigDecimal
import java.util.ArrayList

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table


@Table
@Entity
data class PricingMasterData (
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @OneToMany(orphanRemoval = true, cascade = [ALL])
    var pricingDetails: List<PricingDetail> = ArrayList(),

    @Column
    var sumValue: BigDecimal? = null,

    @Column
    var fundingValue: BigDecimal? = null

)