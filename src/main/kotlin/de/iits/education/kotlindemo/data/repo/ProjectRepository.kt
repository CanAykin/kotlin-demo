package de.iits.education.kotlindemo.data.repo


import de.iits.education.kotlindemo.data.Project
import org.springframework.data.repository.CrudRepository


interface ProjectRepository : CrudRepository<Project, Long> {

    override fun findAll(): List<Project>
}