package de.iits.education.kotlindemo.data

import de.iits.education.kotlindemo.data.district.LocalAuthorityDistrict
import de.iits.education.kotlindemo.data.pricing.PricingMasterData
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToOne
import javax.persistence.Table


@Entity
@Table
data class Project (

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    var projectMasterData: ProjectMasterData? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    var localAuthorityDistrict: LocalAuthorityDistrict? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    var pricingMasterData: PricingMasterData? = null
)