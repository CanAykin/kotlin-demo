package de.iits.education.kotlindemo.data.pricing

enum class PricingItemTypeEnum (private var germanLabel: String){

    ACCESS_EMPTY_PIPES("Zugang zu Leerrohren"),
    DEBUNDLED_ACCESS("entbündelter Zugang"),
    BITSTREAM_ACCESS("Bitstromzugang"),
    ACCESSDARKFIBER("accessDarkFiber"),
    SHARED_USE("gemeinsame Nutzung der physischen Masten"),
    BACKHAULNET_WORKACCESS("Zugang zu Backhaulnetzen");
}