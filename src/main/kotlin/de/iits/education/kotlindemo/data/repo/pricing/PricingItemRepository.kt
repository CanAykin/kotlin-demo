package de.iits.education.kotlindemo.data.repo.pricing


import de.iits.education.kotlindemo.data.Project
import de.iits.education.kotlindemo.data.pricing.PricingItem
import de.iits.education.kotlindemo.data.pricing.PricingMasterData
import org.springframework.data.repository.CrudRepository

interface PricingItemRepository : CrudRepository<PricingItem, Long> {
    override fun findAll(): List<PricingItem>
}