package de.iits.education.kotlindemo.data.pricing

import java.math.BigDecimal

import javax.persistence.Basic
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Table
@Entity
class PricingItem (

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @Column
    @Enumerated(EnumType.STRING)
    var pricingItemType: PricingItemTypeEnum? = null,

    @Column
    var price: BigDecimal? = null
)