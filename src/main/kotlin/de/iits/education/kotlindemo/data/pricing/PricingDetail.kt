package de.iits.education.kotlindemo.data.pricing


import javax.persistence.CascadeType.ALL

import java.util.ArrayList

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table



@Table
@Entity
data class PricingDetail (

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @Column
    @Enumerated(EnumType.STRING)
    var pricingDetailType: PricingDetailTypeEnum? = null,

    @OneToMany(orphanRemoval = true, cascade = [ALL])
    var pricingItems : List<PricingItem> = ArrayList()

)