package de.iits.education.kotlindemo.data.pricing

enum class PricingDetailTypeEnum (val germanLabel: String){
    FTTC("FTTC"),
    FTTB_FTTH("FTTB/FTTH"),
    CABLENETWORK("Kable Netzwerk"),
    PASSIVE_NETWORK_INFRASTRUCTURE("Passive Netzinfrastruktur (nur bei FFTX/Kabel -Ausbau)"),
    MOBILE_NETWORK("Mobile/Drahtlose Netze"),
    OTHER("Sonstige Kosten")
}