package de.iits.education.kotlindemo.data.district

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table

import com.fasterxml.jackson.annotation.JsonIgnoreProperties


@Table
@Entity
data class LocalAuthorityDistrict (

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    var name: String? = null,

    @Column
    @Enumerated(EnumType.STRING)
    var type: LocalAuthorityDistrictTypeEnum? = null,

    @Column
    var districtKey: String? = null,

    @JsonIgnoreProperties("hibernateLazyInitializer", "handler")
    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    var contactPerson: ContactPerson? = null,

    @Column
    var district: String? = null,

    @Column
    var governmentDistrict: String? = null
)