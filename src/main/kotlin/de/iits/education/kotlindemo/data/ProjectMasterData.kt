package de.iits.education.kotlindemo.data

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

import de.iits.education.kotlindemo.data.pricing.PricingDetailTypeEnum



@Entity
@Table
data class ProjectMasterData (

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @Column
    var developmentAreaName: String? = null,

    @Column
    var networkOperator: String? = null,

    @Column
    @Enumerated(EnumType.STRING)
    var projectType: PricingDetailTypeEnum? = null

)