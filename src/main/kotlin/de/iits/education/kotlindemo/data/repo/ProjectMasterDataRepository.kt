package de.iits.education.kotlindemo.data.repo


import org.springframework.data.repository.CrudRepository;
import de.iits.education.kotlindemo.data.ProjectMasterData


interface ProjectMasterDataRepository : CrudRepository<ProjectMasterData, Long> {
}