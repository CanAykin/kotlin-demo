package de.iits.education.kotlindemo.data.repo.district

import de.iits.education.kotlindemo.data.district.LocalAuthorityDistrict
import org.springframework.data.repository.CrudRepository

interface LocalAuthorityDistrictRepository : CrudRepository<LocalAuthorityDistrict, Long>