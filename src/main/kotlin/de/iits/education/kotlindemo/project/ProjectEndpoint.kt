package de.iits.education.kotlindemo.project

import org.springframework.http.ResponseEntity
import org.springframework.util.ClassUtils.isPresent
import de.iits.education.kotlindemo.data.Project
import org.springframework.web.client.RestClientException
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import de.iits.education.kotlindemo.data.repo.ProjectRepository
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api")
class ProjectEndpoint (var projectRepository: ProjectRepository){

    private var log = LoggerFactory.getLogger(javaClass)

    @GetMapping("/projects")
    fun retrieveAllProjects(): List<Project> {

        return projectRepository!!.findAll()

    }

    @PostMapping("/projects")
    fun createProject(@RequestBody project: Project): ResponseEntity<Any> {

        val savedProject = projectRepository!!.save(project)

        val location = ServletUriComponentsBuilder

                .fromCurrentRequest()

                .path("/{id}")

                .buildAndExpand(savedProject.id)

                .toUri()

        return ResponseEntity.created(location).build()

    }

    @GetMapping("/projects/{id}")
    fun getProject(@PathVariable id: Long): Project {

        try {

            return projectRepository!!.findById(id)

                    .orElseThrow { RestClientException("Project could not be found in the Database id=$id") }

        } catch (e: Exception) {

            log.info("Project not found", e)

            throw RestClientException("Project could not be loaded from Database", e)

        }

    }

    @DeleteMapping("/projects/{id}")
    fun deleteProject(@PathVariable id: Long): ResponseEntity<*> {

        try {

            projectRepository!!.deleteById(id)

        } catch (e: Exception) {

            log.info("Project could not be deleted", e)

            return ResponseEntity.badRequest().body(e.message)

        }

        return ResponseEntity.accepted().body("Project deleted projectId=$id")

    }

    @PutMapping("/projects/{id}")
    fun updateProject(@RequestBody project: Project, @PathVariable id: Long): ResponseEntity<Any> {

        if (!projectRepository!!.findById(id).isPresent) {

            return ResponseEntity.notFound().build()

        }

        project.id = id

        projectRepository.save(project)

        return ResponseEntity.noContent().build()

    }

}