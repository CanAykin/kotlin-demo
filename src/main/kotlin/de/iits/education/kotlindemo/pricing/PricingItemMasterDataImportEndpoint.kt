package de.iits.education.kotlindemo.pricing

import de.iits.education.kotlindemo.data.pricing.PricingItem
import java.math.BigDecimal
import de.iits.education.kotlindemo.data.pricing.PricingItemTypeEnum
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.GetMapping
import de.iits.education.kotlindemo.data.repo.pricing.PricingItemRepository
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/masterdata/pricing")
class PricingItemMasterDataImportEndpoint ( var pricingItemRepository : PricingItemRepository){

    @GetMapping
    fun getPricingItemPrices(): Map<PricingItemTypeEnum?, BigDecimal?> {
        return pricingItemRepository.findAll().map { it.pricingItemType to it.price }.toMap()
    }

    @PostMapping
    fun updatePrices(@RequestBody newPrices: Map<PricingItemTypeEnum, BigDecimal>): ResponseEntity<Any> {
        val currentPrices = pricingItemRepository!!.findAll()
        currentPrices.forEach { price -> updatePrice(newPrices, price) }
        pricingItemRepository.saveAll(currentPrices)
        return ResponseEntity.ok().build()
    }

    private fun updatePrice(newPrices: Map<PricingItemTypeEnum, BigDecimal>, price: PricingItem) {
        price.price = newPrices[price.pricingItemType]
    }
}