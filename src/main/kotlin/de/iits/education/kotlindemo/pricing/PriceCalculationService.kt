package de.iits.education.kotlindemo.pricing

import de.iits.education.kotlindemo.data.Project
import java.math.BigDecimal
import de.iits.education.kotlindemo.data.pricing.PricingItem
import de.iits.education.kotlindemo.data.pricing.PricingMasterData
import de.iits.education.kotlindemo.data.repo.pricing.PricingMasterDataRepository
import de.iits.education.kotlindemo.data.repo.ProjectRepository
import org.springframework.stereotype.Component
import java.util.function.ToDoubleFunction


@Component
class PriceCalculationService (
        var projectRepository: ProjectRepository,
        var pricingMasterDataRepository: PricingMasterDataRepository
        )
{

    fun calculateAndUpdatePrice(projectId: Long): PricingMasterData {
        val project = findProject(projectId)
        val pricingMasterData = project.pricingMasterData
        val sum = calculateSum(pricingMasterData!!)
        pricingMasterData.sumValue = BigDecimal(sum)
        pricingMasterDataRepository.save(pricingMasterData)
        return pricingMasterData
    }

    private fun calculateSum(pricingMasterData: PricingMasterData): Double {
        return pricingMasterData.pricingDetails
                .flatMap { it.pricingItems }
                .map{getPricingItemValue(it)}
                .sum()
    }

    private fun getPricingItemValue(pricingItem: PricingItem): Double {
        return pricingItem.price?.toDouble()?: 0.0
    }

    private fun findProject(projectId: Long): Project {
        return projectRepository.findById(projectId)
                .orElseThrow { UnsupportedOperationException("Project could not be found inside the database projectID=$projectId") }
    }
}

