package de.iits.education.kotlindemo.pricing

import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api")
class PricingMasterDataEndpoint {
    var priceCalculationService: PriceCalculationService? = null
    @PostMapping("pricingMasterData/calculateAndUpdateSum/{projectId}")
    fun calculateAndUpdateSum(@PathVariable projectId: Long): Double {
        val pricingMasterData = priceCalculationService!!.calculateAndUpdatePrice(projectId)
        return pricingMasterData.sumValue!!.toDouble()
    }
}