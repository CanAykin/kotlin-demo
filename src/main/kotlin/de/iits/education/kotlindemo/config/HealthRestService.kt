package de.iits.education.kotlindemo.config

import org.springframework.boot.actuate.health.CompositeHealth
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HealthRestService(){

    @GetMapping("/")
    fun ok(){
    }
}