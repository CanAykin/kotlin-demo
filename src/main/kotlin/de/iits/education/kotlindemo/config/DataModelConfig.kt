package de.iits.education.kotlindemo.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackages = ["de.iits.education.kotlindemo.data.repo"])
@EntityScan(basePackages = ["de.iits.education.kotlindemo.data"])
@EnableTransactionManagement
class DataModelConfig