package de.iits.education.kotlindemo.config

import org.springframework.context.annotation.Configuration

import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer

@Configuration
class RepositoryRestConfig : RepositoryRestConfigurer {

    override fun configureRepositoryRestConfiguration(restConfig: RepositoryRestConfiguration) {
        restConfig.setBasePath("api/repositories")
    }
}